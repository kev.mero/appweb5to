function calculo() {
    var numero1 = document.getElementById("n1").value
    var numero2 = document.getElementById("n2").value
    var contador = 1;

    if (numero1.length == "" || numero2.length == "") {
        error();
    } else {
        var n1 = parseInt(numero1);
        var n2 = parseInt(numero2);
        do {
            if (contador == 1) {
                document.getElementById("suma").value = n1 + n2;
                contador++;
            }
            if (contador == 2) {
                document.getElementById("resta").value = n1 - n2;
                contador++;
            }
            if (contador == 3) {
                document.getElementById("multiplicacion").value = n1 * n2;
                contador++;
            }
            if (contador == 4) {
                document.getElementById("division").value = n1 / n2;
                contador++;
            }
            if (contador == 5) {
                document.getElementById("mod").value = n1 % n2;
                contador++;
            }

        } while (contador <= 5);

        success();

        document.getElementById("n1").value = "";
        document.getElementById("n2").value = "";
    }

}


function success() {
    Swal.fire({
        title: 'Correcto',
        icon: 'success',
        showConfirmButton: false,
        timer: 500
    })
}


function error() {
    Swal.fire({
        title: 'Error',
        text: 'Campos vacios',
        icon: 'error',
        showConfirmButton: false,
        timer: 500
    })
}