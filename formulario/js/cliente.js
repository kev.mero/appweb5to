var lista_cliente = [];

function CrearListaCliente(cedula, apellidos, nombres, estadocivil, sexo, ciudad, direccion, telefono, email) {

    var Cliente = {
        cedula: cedula,
        apellidos: apellidos,
        nombres: nombres,
        estadocivil: estadocivil,
        sexo: sexo,
        ciudad: ciudad,
        direccion: direccion,
        telefono: telefono,
        email: email
    }
    lista_cliente.push(Cliente);

}

function ObtenerListaClientes() {


    var listaCliente = localStorage.getItem('ListaCliente');
    if (listaCliente == null) {
        lista_cliente = [];
    } else {
        lista_cliente = JSON.parse(listaCliente);
    }
    return lista_cliente;

}