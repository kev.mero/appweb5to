


function aggCliente() {

    var cedula = document.getElementById("cedula").value;
    var apellidos = document.getElementById("apellidos").value;
    var nombres = document.getElementById("nombres").value;
    var estadocivil = document.getElementById("estadocivil").value;
    var sexo = document.getElementById("sexo").value;
    var ciudad = document.getElementById("ciudad").value;
    var direccion = document.getElementById("direccion").value;
    var telefono = document.getElementById("telefono").value;
    var email = document.getElementById("email").value;




    if (cedula.length == 0 || apellidos.length == 0 || nombres.length == 0 || estadocivil.length == 0 ||
           sexo.length == 0 || ciudad.length == 0 || direccion.length == 0 || telefono.length == 0 || email.length == 0) {
        error();
    } else {

         CrearListaCliente(cedula, apellidos, nombres, estadocivil, sexo, ciudad, direccion, telefono, email)

         localStorage.setItem('ListaCliente', JSON.stringify(lista_cliente));

         obtenerClientes();
         success();

        document.getElementById("cedula").value = "";
        document.getElementById("apellidos").value = "";
        document.getElementById("nombres").value = "";
        document.getElementById("ciudad").value = "";
        document.getElementById("direccion").value = "";
        document.getElementById("telefono").value = "";
        document.getElementById("email").value = "";

    }
}



function obtenerClientes() {

    var list = ObtenerListaClientes(),
        tbody = document.querySelector('#tabla-clientes tbody');

    tbody.innerHTML = '';
    for (let i = 0; i < list.length; i++) {
        var row = tbody.insertRow(i),

            cedula = row.insertCell(0),
            apellidos = row.insertCell(1),
            nombres = row.insertCell(2),
            estadocivil = row.insertCell(3),
            sexo = row.insertCell(4);
            ciudad = row.insertCell(5),
            direccion = row.insertCell(6),
            telefono = row.insertCell(7),
            email = row.insertCell(8);


        cedula.innerHTML = list[i].cedula;
        apellidos.innerHTML = list[i].apellidos;
        nombres.innerHTML = list[i].nombres;
        estadocivil.innerHTML = list[i].estadocivil;
        sexo.innerHTML = list[i].sexo;
        ciudad.innerHTML = list[i].ciudad;
        direccion.innerHTML = list[i].direccion;
        telefono.innerHTML = list[i].telefono;
        email.innerHTML = list[i].email;

        tbody.appendChild(row);
    }
}




function success() {
    Swal.fire({
        title: 'Correcto',
        text: 'Registro exitoso',
        icon: 'success',
        showConfirmButton: false,
        timer: 1000
    })
}

function error() {
    Swal.fire({
        title: 'Error',
        text: 'Ingrese todos los datos',
        icon: 'error',
        showConfirmButton: false,
        timer: 1000
    })
}


