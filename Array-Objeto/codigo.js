
//Creacion de objeto

function Producto_alimenticio(codigo, nombre, precio) {

    this.codigo = codigo;
    this.nombre = nombre;
    this.precio = precio;

}


// guardar en array productos 

var productoArray = new Array();

productoArray[0] = new Producto_alimenticio(1, "Galleta", 0.50);
productoArray[1] = new Producto_alimenticio(2, "Helado", 0.25);
productoArray[2] = new Producto_alimenticio(3, "Gaseosa", 0.60);




// funcion imprimir datos
function imprimirDatos() {

    productoArray.forEach(producto => {

        document.write(`<h1 style="text-align:center;">Registro ${productoArray.indexOf(producto)}</h1>`)
        document.write(`<p><strong>Codigo:</strong> ${producto.codigo}</p>`)
        document.write(`<p><strong>Nombre:</strong> ${producto.nombre}</p>`)
        document.write(`<p><strong>Precio:</strong> ${producto.precio}</p>`)
        document.write(`<hr></hr>`)
    });
}